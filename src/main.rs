use serde::{Deserialize, Serialize};
use clap::{App, Arg};
use failure::Error;
use semver::Version;
use std::fs;
use subprocess::{Exec, Pipeline, Redirection};
use git2::{Commit, Repository, Tag};

#[derive(Debug, Deserialize, Serialize)]
struct GitVersionOutput {
    Major: usize,
    Minor: usize,
    Patch: usize,
    PreReleaseTag: String,
    PreReleaseTagWithDash: String,
    PreReleaseLabel: String,
    PreReleaseNumber: usize,
    WeightedPreReleaseNumber: usize,
    BuildMetaData: String,
    BuildMetaDataPadded: String,
    FullBuildMetaData: String,
    MajorMinorPatch: String,
    SemVer: Version,
    LegacySemVer: String,
    LegacySemVerPadded: String,
    AssemblySemVer: String,
    AssemblySemFileVer: String,
    FullSemVer: Version,
    InformationalVersion: Version,
    BranchName: String,
    Sha: String,
    ShortSha: String,
    NuGetVersionV2: String,
    NuGetVersion: String,
    NuGetPreReleaseTagV2: String,
    NuGetPreReleaseTag: String,
    VersionSourceSha: String,
    CommitsSinceVersionSource: usize,
    CommitsSinceVersionSourcePadded: String,
    CommitDate: String
}

fn get_gitversion_output() -> Result<GitVersionOutput, Error> {
    let output = Exec::cmd("mono")
        .arg("/home/lkeiser/Dev/Tools/GitVersion/GitVersion.exe")
        .stdout(Redirection::Pipe)
        .capture()?.stdout_str();

    let output= serde_json::from_str::<GitVersionOutput>(&output)?;

    Ok(output)
}

fn set_tag(name: Version, msg: Option<&str>, force: bool) -> Result<(), Error> {
    let repository = Repository::open(".")?;

    let target = "HEAD";
    let obj = repository.revparse_single(target)?;

    if let Some(ref message) = msg {
        let sig = repository.signature()?;
        repository.tag(&name.to_string(), &obj, &sig, message, force)?;
    } else {
        repository.tag_lightweight(&name.to_string(), &obj, force)?;
    }

    Ok(())

}

fn main() {
    dbg!(get_gitversion_output().unwrap());
    set_tag(get_gitversion_output().unwrap().FullSemVer, None, false).unwrap();
}
